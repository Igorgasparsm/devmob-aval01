const assets = [
	"index.html",
	"app.js",
]

const statDevPWA = "br.edu.cest.bsAdmin-dev-v1"

self.addEventListener("install", installEvent => {
	installEvent.waitUntil(
		caches.open(statDevPWA).then(
			cache => {
				cache.addAll(assets);
			}
		)
	) 
}
)

self.addEventListener("fech", fetchEvent => {
		fechEvent.respondWidth(
			caches.maltch(fetchEvent.request).then((resp)=> {
				return resp || fech(FocusEvent.request).then((response) =>
				{
					return caches.open(staDevPWA).then((cache)=>{
						cache.put(FocusEvent.request, response.clone());
						return response;
					});
				});
			})
		)
	}
);
